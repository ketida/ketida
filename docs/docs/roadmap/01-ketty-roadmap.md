---
title: 'Roadmap'
---

At Ketty, we’re constantly improving our platform to make modern book publishing more collaborative, intuitive, and efficient. You can see what's included in each release from the [Ketty Gitlab Releases page](https://gitlab.coko.foundation/coko-org/products/ketty/ketty/-/releases), and below is an outline of what we’re working on now and some of the exciting features planned for the future.

## What we’re focusing on now

### Localisation support
We’re enabling support for multiple languages, so your team can work in the language that suits them best. This includes tools to customise translations and adapt Ketty to diverse audiences.

### Template manager (V1)
We’re introducing tools that allow you to upload and manage custom templates within your Ketty instance. 

### Maintenance
We’re making behind-the-scenes upgrades to improve performance and fix bugs, ensuring Ketty remains fast and reliable.

## What’s coming up next
Here’s a sneak peek at some features on the horizon. Timelines and release order may shift based on user needs and community support, but these are the projects we’re excited about:

### Customisable editing tools
You’ll be able to adjust the editing toolbar to suit your project needs, turning on only the tools you need and hiding the rest. This feature will help simplify workflows, maintain design consistency, and empower you to create professional-quality content with ease and flexibility.

This includes reintroducing several **advanced editor features** including tables, footnotes and endnotes, textbook block elements, highlighting, superscript and subscript, MathJax for scientific notation, and code blocks. We’re **updating all existing templates** to support new styles, ensuring consistency across all publishing formats.

### Collaborative editing
Multiple collaborators will soon be able to edit the same chapter simultaneously. This will make teamwork seamless, with tools to track and manage comments as you work.

### User management
Admins will have more control, with the ability to view and manage user accounts, including approving sign-ups or deactivating accounts when needed.

### AI-powered open textbook planner
We’re reinventing the Open Textbook Planner with AI tools to make creating textbooks faster and smarter. 

## Reaching the Ketty V3 milestone

### Expanded publishing options
You’ll have more control over book design, including numbering chapters or parts, and refining styles directly in the editor. 

### Updated version of the AI Book Designer
You’ll be able to use AI to adjust your book’s layout directly from an existing template to fine-tune your print-ready PDFs. 

### Template manager (V2)
Extending the template manager to support custome styles (associated to custom inline and block-level styles applied in the editor) giving you more flexibility and control over your book’s design.

### Asset manager
The Asset Manager will let you organise and manage all the images in your book from one place. This feature includes AI-powered tools to generate alternative text for images, improving accessibility and making it easier to meet publishing standards.

*These features mark a major milestone: Ketty V3 will complete the transfer of all critical functionality from Ketty V1 (Editoria) while enhancing usability and performance.*


## Looking to the future
We’re committed to shaping Ketty based on community feedback. Here’s a look at features we hope to build with your input and support:

- **More templates**: Expanded options for front matter, back matter, and interactive elements like questions and quizzes.
- **AI-driven template maker**: Design templates from scratch with tools for non-technical designers and CCS power users alike. 
- **Single sign-on**: Simplify access for teams and organisations with SSO integration.


## Get involved
We can’t do this without you! Community support helps us prioritise and bring new features to life, including enhanced AI tools and more template options. If you’d like to contribute or share your ideas, let us know at [Ketty Community Forum](https://forum.ketty.community).

Together, we’re building the future of publishing.