---
title: 'Community Collaboration Process'
---

Ketty is a community project so it’s crucial that we collaborate on the roadmap so that the platform is developed in a direction that benefits the entire community. When community proposals are ahead of the development team, it can actually accelerate the feature development and delivery process, as there will be less need for large-scale refactors, more  opportunity for cross-organisational collaboration of specific features, and a reduced likelihood of duplicate efforts. This article outlines how we collaborate on feature proposals to ensure that all parties involved have an opportunity to share their input before key features are developed.

## 1. Get familiar with the roadmap

The Coko team maintains a [public roadmap](../roadmap/01-ketty-roadmap.md) for Ketty. This roadmap aims to provide a high-level overview of Ketty’s direction. Check it out. Ask us questions about it. It's likely that the feature you're looking for is already planned.

## 2. Share your ideas for Ketty’s roadmap

If you have a use case that isn’t included on our roadmap, please share your ideas by contacting us at ketty@coko.foundation or opening a feature proposal in the [Ketty Forum](https://forum.ketty.community/) or Gitlab project.

:::info
Your feature proposal should include the following information:

1. **Context:** Give the necessary context for your proposal. For example: what problem will this feature solve for users? What are the use cases, benefits, and goals?
2. **Proposal:** Give a precise statement of the proposed feature. Add as much detail as possible.
3. **Design:** A picture is worth a thousand words. If you're able to, include rough sketches or wireframes of the UI suggested for this feature.
   :::

## 3. Coko team and community feedback on new proposals

The Coko team will respond to your proposal as soon as possible to discuss the idea and ask clarifying questions with the aim to make the use case understandable to the Ketty community. Following this, we’ll announce the feature proposal in the Ketty forum and other channels, inviting other community members to give comments.

## 4. Accepting proposals to the roadmap

The Coko team will accept the proposal to the roadmap and announce this to the Ketty community. If there isn't one already, we’ll create an issue in the Gitlab repository (where the development is managed), and link it to the feature proposals topic in the Ketty forum so that users can keep up-to-date on its progress.

## 5. Community development collaboration

If the proposal also includes development effort support, the following process in Gitlab applies:

1. The Ketty lead developer provides implementation guidance in the Gitlab issue.
2. The community developer opens a branch for the feature.
3. The community developer should push local work to this branch regularly to avoid lost work.
4. When the feature is ready for review the community developer opens an MR and assigns the Ketty lead developer.
5. The Ketty team (Lead Developer, QA Engineer and Project Manager) review the code and functionality.
6. The Ketty lead developer may request changes for the community developer to implement.
7. The lead developer merges the code.
8. The feature is released and announced to the community.

:::info
Contributing developers should refer to the [Coko Dev Docs](https://devdocs.coko.app/) (for general set up and common libraries guides) and the [Ketty Developer Guide](../developerGuide/01-Repositories%20&%20Setup.md).
:::
