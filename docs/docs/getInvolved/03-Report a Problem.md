---
title: 'Report a Problem'
---

Found something that's not working as it should in Ketty? Please let us know by sharing the information in a bug report.

## Reporting bugs

You can share you bug report by:

1. Contacting us by email at ketty@coko.foundation
2. Openining a bug report as an issue in the Ketty Gitlab project [here](https://gitlab.coko.foundation/coko-org/products/ketty/ketty/-/issues).

## Bug report information

The more information you provide, the easier it will be for us to resolve the problem. Here's a description of what a good bug report includes:

1. **Expected behaviour:** This is really important. What should have happen that didn't? Can you link to the user guide section that describes the behaviour you were expecting?
2. **Current behaviour:** Tell us what happens instead of the expected behaviour.
3. **Steps to reproduce:** In as much detail as possible, describe the steps that led to the issues you are reporting. If possible, provide a link to a live example or screenshots demonstrating the issue.
4. **Environment:** At a minimum, let us know the URL of the instance where the problem occured. If you can, share your username and browser name and version.
5. **Error logs:** If you're familiar with your browser inspection tool, share the console logs.
