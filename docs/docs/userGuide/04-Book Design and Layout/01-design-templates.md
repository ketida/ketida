---
title: 'Design and Layout Templates'
---

## Available templates 

Ketty includes eight built-in design and layout templates that provide styling across PDF, EPUB, and web formats:

1. **Vanilla**
2. **Atosh**
3. **Bikini**
4. **Eclypse**
5. **Slategrey**
6. **Logical**
7. **Significa**
8. **Tenberg**

## Multi-format consistency

One of Ketty's strengths is maintaining design consistency across formats. The eight built-in templates ensure that your book maintains a cohesive look whether viewed as:
- A PDF for print or screen
- An EPUB for e-readers
- A web version for online reading

While each format may have specific optimisations, the core design elements remain consistent through the template system.

## Automated typesetting

Ketty uses Paged.js as its automated typesetting engine for PDF generation. Through the templates, Paged.js controls fundamental aspects of your book's design. It manages typography by handling font selections and their scaling across different elements. For page layout, it determines aspects like margin sizes and running headers. It handles how content is presented, including the styling of elements like block quotes, and takes care of special elements such as part and chapter title treatments. This automated approach ensures consistent, professional-quality typesetting throughout your book.
