---
title: 'Print-on-Demand Publishing'
---

## Connecting with Lulu

Ketty integrates with Lulu.com through the publishing profiles. Both PDF and EPUB formats can be distributed through Lulu. 

1. Navigate to the **Preview and Publish** page
2. Select the "Publishing Profiles" tab
3. Choose a PDF or EPUB profile from the dropdown menu

:::note
Only Book Owners can connect to print-on-demand services.
:::

### Initial connection

After selecting the relevant profile: 
1. Select "Connect to Lulu"
2. Complete the Lulu authentication process
3. Return to Ketty to finish setup

### Upload process

After the initial connection: 
1. Select "Upload to Lulu" to send to Lulu
2. Wait for sync confirmation

### Managing sync status

Profiles can show different sync states:
- **Synced:** Content matches POD platform
- **Out of sync:** Changes detected
- **Never synced:** Initial upload needed

:::note
Changes made to publishing profiles don't automatically sync with POD services. You'll need to manually update the POD version when ready.
:::