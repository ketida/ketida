---
title: 'Export Options'
---

## Export formats

Ketty supports exporting your book as a PDF and EPUB through publishing profiles. 

1. Navigate to the **Preview and Publish** page
2. Select the "Publishing Profiles" tab
3. Choose a PDF or EPUB profile from the dropdown menu
4. Review the profile
5. Select "Download"

### PDF export

- Multiple page size options
- Configurable front matter
- Print-ready output

### EPUB export

- Validated with EPUBcheck agaisnt the EPUB 3.3 specification
- Embedded metadata
- ISBN integration
- Optional cover inclusion

:::tip
Always download and review exported files before distribution to ensure quality and formatting accuracy. For EPUBs, test in various e-readers to verify compatibility.
:::

