---
title: 'AI Writing Assistance'
---

## Enable AI writing

The Book Owner can enable AI writing assistance for all collaborators with edit access. To enable this feature:

1. Open **Book Settings** (cog icon in the top navigation)
2. Turn on "AI writing prompt use"
3. Configure custom prompts if desired

:::note
Your Ketty instance must have an AI key configured by an administrator before this functionality can be used.
:::

## Using AI prompts

Once enabled, the AI writing assistant is accessible through the editor toolbar:

1. Select text in your chapter
2. Click the AI assistant icon
3. Type your prompt
4. Review the generated response
5. Choose to:
   - Replace selected text
   - Insert new text
   - Discard the suggestion

## Custom prompts

Book Owners can create predefined prompts to:
- Ensure consistent AI assistance across the book
- Guide collaborators toward specific writing goals
- Streamline common writing tasks

### Managing custom prompts

- Add any number of custom prompts
- Optionally disable free-text prompts
- Edit or remove prompts as needed

## Best practices

1. **Effective prompting**
   - Be specific in your requests
   - Provide context when needed
   - Break complex tasks into smaller prompts
2. **Review and refine**
   - Always review AI-generated content
   - Edit for tone and style consistency
   - Use AI as a writing aid, not a replacement
3. **Collaboration**
   - Communicate AI usage with collaborators
   - Establish guidelines for AI-assisted content
   - Maintain consistent writing quality
