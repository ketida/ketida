---
title: 'Working with Images'
---

## Adding images

To add images to your book:

1. Place your cursor where you want the image to appear
2. Select the upload image icon from the toolbar
3. Choose your image file from your computer
4. Wait for the upload to complete

## Supported formats

Ketty supports multiple image formats:
- JPEG
- PNG
- WEPB
- TIFF

:::note
TIFF files are automatically converted by Ketty for optimal performance. In EPUB and PDF exports, size-optimised versions of images are used.
:::

## Image properties

After inserting an image, you can enhance it with:

### Captions

- Click in the caption input area below the image to add a caption
- Captions appear in your exports and help readers understand the image content

### Alt text

- Click on the image to make the alt text input area visible 
- Alt text improves accessibility and helps screen readers describe images
- Keep alt text concise but descriptive

## Managing images

To to delete an image, click on the image and press backspace 

:::note
Image sizing is controlled by your selected design template. You do not need to manually resize images in the editor.
:::

## Best practices

1. **Image quality**
   - Use high-resolution images for best print results
   - Provide images in their original proportions
2. **Accessibility**
   - Always include meaningful alt text
   - Use captions to provide context
   - Ensure sufficient contrast in images
3. **Organisation**
   - Place images within the same section as the relevant text
   - Include figure references in the captions and refer to these in text (for example 'See Figure 1.5')


