---
title: 'Creating Your Account'
---

## Before you begin

To use Ketty, you'll need:
- An email address
- The URL for your Ketty instance (provided by your administrator)
- Permission to accept the instance's terms and conditions

## Creating an account

### Sign up process

1. Navigate to the sign up URL of your Ketty instance
2. Enter your information:
   - Full name
   - Email address
   - Password
3. Review and accept the terms and conditions
4. Select "Sign Up"
5. Check your email for a verification link
6. Click the verification link to activate your account

### Password guidelines

Choose a secure password that you haven't used for other accounts. Your administrator may have specific password requirements for your instance.

## Accessing your account

### Logging in

1. Visit your Ketty instance URL
2. Enter your email and password
3. Select "Log In"

### Managing your password

If you forget your password:
1. Select "Forgot your password?" from the login page
2. Enter your email address
3. Check your email for reset instructions
4. Follow the link to create a new password

## Account security

### Logging out

To protect your account when using shared devices:
1. Select your username initials in the top right corner
2. Choose "Logout"

### Best practices

- Log out when using shared computers
- Don't share your account credentials
- Contact your administrator if you notice suspicious activity

## Getting help

If you encounter issues with your account:
- Check any error messages for specific guidance
- Contact your instance administrator
- Review the documentation for additional help



