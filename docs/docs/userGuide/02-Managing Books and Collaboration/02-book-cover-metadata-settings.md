---
title: "Book Cover, Metadata, and Settings"
---

## Accessing book metadata

The **Book Metadata** page can be accessed in two ways:
1. From the **Dashboard**: select "Manage cover and metadata" from the three dots next to the book's title
2. From the **Producer** page: select "Book Metadata" in the left sidebar

## Managing book cover

From the **Book Metadata** page, you can:
- Upload a cover image
- Provide alt text for the cover image
- Delete or replace a cover image

### Cover image usage

The cover image is automatically included in:
- The web preview and published website
- The EPUB export (exclude the cover on the **Preview and Publish** page)

Supported image formats:
- JPEG
- PNG
- WEBP

:::note
The cover image is not included in the PDF export. If you want to print your book with a print-on-demand supplier, the supplier will request a cover that is created seperately from the interior of the book. The PDF export from Ketty includes the interior of the book only.
:::

## Managing front matter metadata

### Title page information

Configure the core details of your book:
- Title
- Subtitle
- Authors

### Copyright page information

Add publishing and rights information:

#### ISBNs

- Add multiple ISBNs with unique labels
- Each ISBN can include a custom label (useful for different formats and regions)

#### Copyright license

- Choose from various license options
- Some licenses include additional configuration options

## Book settings

The Book Owner can control specific book settings from the **Producer** page. Access settings by clicking the cog icon in the top navigation.

### AI writing assistance

Control AI features for your book:
- Enable/disable AI writing prompts
- Configure custom prompts
- Control free-text prompt availability

### AI Book Designer

Manage the AI design features:
- Enable/disable the AI Book Designer feature
- Access design tools from the top navigation
- Configure design preferences

### Knowledge Base

Control reference materials for AI assistance:
- Enable/disable the Knowledge Base feature
- Manage reference materials
