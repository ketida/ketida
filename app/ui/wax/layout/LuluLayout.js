/* stylelint-disable no-descending-specificity */
import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled, { ThemeProvider, css } from 'styled-components'
import { grid, th } from '@coko/client'
import { Spin } from 'antd'
import {
  ToTopOutlined,
  CaretUpFilled,
  CaretDownFilled,
} from '@ant-design/icons'
import {
  ApplicationContext,
  WaxContext,
  ComponentPlugin,
  WaxView,
  DocumentHelpers,
} from 'wax-prosemirror-core'
import { useTranslation } from 'react-i18next'
import { usePrevious } from '../../../utils'
import { Button, Checkbox } from '../../common'
import BookPanel from '../../bookPanel/BookPanel'
import {
  BookInformation,
  BookMetadataForm,
  SettingsForm,
  UserInviteModal,
} from '../../bookInformation'
import theme from '../../../theme'

import 'wax-prosemirror-core/dist/index.css'
import 'wax-prosemirror-services/dist/index.css'
import 'wax-table-service/dist/index.css'

// #region styled
const Wrapper = styled.div`
  --top-menu-base: clamp(3rem, 4.3478rem + -1.7391vw, 4rem);
  background: ${th('colorBackground')};
  display: flex;
  flex-direction: column;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBase')};
  height: 100%;
  overflow: hidden;
  width: 100%;
`

const Main = styled.div`
  display: flex;
  flex: 1 1 calc(100% - var(--top-menu-base));
  overflow: hidden;
  position: relative;
  width: 100%;

  > :nth-child(2) {
    overflow: auto;
    width: 100%;
  }
`

const StyledMetadataForm = styled(BookMetadataForm)`
  padding-inline-start: calc(50px + var(--s1));

  @media (min-width: 600px) {
    padding-inline-start: var(--s1);
  }
`

const TopMenu = styled.div`
  align-items: center;
  background-color: ${th('colorBackground')};
  border-bottom: 1px solid lightgrey;
  display: flex;
  flex: 1 0 var(--top-menu-base);
  flex-flow: nowrap;
  gap: ${grid(1)};
  justify-content: center;

  ${({ isHidden }) =>
    isHidden &&
    css`
      > * {
        opacity: 0;
        visibility: hidden;
      }
    `};

  padding: ${grid(2)} ${grid(4)};
  user-select: none;

  &.scrollable {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
    row-gap: ${grid(2)};
  }

  &.scrollable[data-expanded='false'] {
    flex: unset;
    height: 48px;
    overflow: hidden;
  }

  > div {
    display: contents;
    justify-content: center;

    &:has(button[title='Undo']) {
      display: inline-flex;
    }

    > div {
      text-align: center;
    }

    &:has(#block-level-options) {
      display: flex;
    }
  }

  [aria-controls='block-level-options'] {
    background-color: transparent;
    width: 90px;
  }

  #block-level-options {
    width: 100px;
    z-index: 1001;
  }

  > div > div:has(#custom-block-level-options) button[aria-haspopup='true'] {
    width: 120px;
  }

  .Dropdown-root {
    display: contents;
  }

  .Dropdown-control {
    align-items: center;
    display: flex;
    justify-content: space-between;
    padding: 8px;
    white-space: nowrap;
    width: 120px;

    .Dropdown-arrow {
      position: unset;
    }
  }

  .Dropdown-menu {
    top: unset;
    width: 120px;
    z-index: 1001;
  }

  [aria-controls='table-options'] {
    width: 120px;
  }

  #table-options {
    span {
      text-align: start;
    }
  }

  &[data-loading='true'] [aria-controls='block-level-options'] {
    > span {
      opacity: 0;
    }
  }

  #collapse {
    align-items: center;
    border: none;
    box-shadow: none;
    display: none;
    flex-direction: row-reverse;
    justify-content: center;

    > .ant-btn-icon {
      margin-inline: ${grid(2)} 0;
    }
  }

  &.scrollable #collapse {
    display: flex;
  }
`

const CollapseContainer = styled.div`
  background-color: transparent;
  display: flex;
  inset-inline-end: ${grid(3)};
  justify-content: center;
  padding-block-start: 9px;
  position: absolute;
  z-index: 9;

  &[data-collapsed='true'] {
    align-items: start;
    background-color: white;
    height: unset;
    inset: 0;

    button {
      transform: rotate(90deg);
    }
  }

  button {
    transform: rotate(-90deg);
    transition: transform 0.3s ease-out;
  }

  @media (min-width: 800px) {
    display: none;
  }
`

const EditorArea = styled.div`
  background: #e8e8e8;
  border-bottom: 1px solid lightgrey;
  flex-grow: 1;
  height: 100%;
  padding: 4px 0 0;
  width: ${({ isFullscreen }) => (isFullscreen ? '100%' : '80%')};
`

const WaxSurfaceScroll = styled.div`
  box-sizing: border-box;
  display: flex;
  height: 100%;
  overflow-y: auto;
  position: relative;
  width: 100%;
`

const CommentsContainer = styled.div`
  display: flex;
  flex: 1 0 calc(205px + 1em);
  flex-direction: column;
  height: 100%;
  position: relative;
  width: calc(205px + 1em);

  @media (max-width: 1400px) {
    position: absolute;
    right: ${grid(1)};
  }

  > div {
    margin-inline-start: 1em;
  }

  textarea {
    border: 1px solid ${th('colorBorder')};
  }

  button {
    border-radius: 3px;
  }

  &:empty {
    display: none;
  }
`

const TrackToolsContainer = styled.div`
  border: 1px solid ${th('colorBorder')};
  display: grid;
  grid-auto-rows: 30px;
  grid-template-columns: 1fr;
  margin-inline-start: 0;
  position: fixed;
  right: clamp(0rem, -0.2174rem + 1.087vw, 0.625rem);
  z-index: 1;
`

const ToggleComments = styled.div`
  align-items: center;
  background-color: ${th('colorBackground')};
  border-bottom: 1px solid ${th('colorBorder')};
  display: inline-flex;
  padding-inline: ${grid(2)};

  > label {
    flex-direction: row-reverse;

    .ant-checkbox {
      margin-inline: 6px;
    }
  }

  @media (min-width: 1400px) {
    display: none;
  }
`

const TrackTools = styled.div`
  align-items: center;
  background-color: ${th('colorBackground')};
  display: flex;
  justify-content: end;
  padding-inline: ${grid(2)};
  position: relative;
  z-index: 1;
`

const TrackOptions = styled.div`
  display: flex;
  margin-left: 10px;
  position: relative;

  > div > button ~ div {
    right: ${grid(-2)};
  }
`

const EditorContainer = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  margin: 0 auto;
  position: relative;
  width: 1016px;

  > div:first-child {
    max-width: 816px;
    width: 100%;
  }

  .ProseMirror {
    --padding-inline: clamp(1.25rem, -0.4022rem + 8.2609vw, 6rem);
    background: ${({ selectedChapterId }) =>
      selectedChapterId ? '#fff' : '#e8e8e8'};
    min-height: calc(100vh - 104px);
    padding: ${grid(20)} var(--padding-inline) ${grid(20)}
      calc(50px + var(--padding-inline));
    width: calc(100% - 20px);

    @media (min-width: 600px) {
      padding: ${grid(20)} var(--padding-inline);
    }

    table > caption {
      caption-side: top;
    }

    figcaption {
      width: 624px;
    }
  }
`

const StyledSpin = styled(Spin)`
  background-color: white;
  display: grid;
  height: 100vh;
  inset: 0;
  justify-content: center;
  margin-inline: auto;
  padding-block-start: 20%;
  position: absolute;
`

const LeftPanelWrapper = styled.div`
  background-color: ${th('colorBackground')};
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
  padding-inline: ${grid(3)};
  position: absolute;
  transition: flex-basis 0.4s, width 0.4s;
  width: 300px;
  z-index: 1000; // hate it but it's the wax cursor's fault!

  &:has([data-collapsed='true']) {
    flex: 0 0 50px;
    width: 50px;
  }

  @media (min-width: 600px) {
    flex: 0 0 300px;
    position: relative;
    width: unset;
  }

  @media (min-width: 800px) {
    &:has([data-collapsed]) {
      flex: 0 0 34%;
    }
  }

  @media (min-width: 1100px) {
    &:has([data-collapsed]) {
      flex: 0 0 380px;
    }
  }
`

const StyledSettingsForm = styled(SettingsForm)`
  padding-inline-start: calc(50px + var(--s1));

  @media (min-width: 600px) {
    padding-inline-start: var(--s1);
  }
`

const NoSelectedChapterWrapper = styled.div`
  display: grid;
  font-size: 16px;
  height: 80%;
  place-content: center;
`
// #endregion styled

const MainMenuToolBar = ComponentPlugin('mainMenuToolBar')
const RightArea = ComponentPlugin('rightArea')
const CommentTrackToolBar = ComponentPlugin('commentTrackToolBar')

const LuluLayout = ({ customProps, ...rest }) => {
  const {
    chapters,
    onDeleteChapter,
    onChapterClick,
    onReorderChapter,
    onUploadChapter,
    onBookComponentTypeChange,
    onBookComponentParentIdChange,
    selectedChapterId,
    title,
    subtitle,
    onAddChapter,
    onSubmitBookMetadata,
    bookMetadataValues,
    chaptersActionInProgress,
    canEdit,
    metadataModalOpen,
    setMetadataModalOpen,
    editorLoading,
    onUploadBookCover,
    viewMetadata,
    setViewMetadata,
    settings,
    getBookSettings,
    bookId,
    aiEnabled,
    savedComments,
  } = customProps

  const [lastSelectedChapter, setLastSelectedChapter] = useState(null)
  const [bookPanelCollapsed, setBookPanelCollapsed] = useState(true)
  const [mobileToolbarCollapsed, setMobileToolbarCollapsed] = useState(true)
  const [showComments, setShowComments] = useState(true)
  const previousComments = usePrevious(savedComments)
  const { t } = useTranslation(null, { keyPrefix: 'pages.producer' })

  const {
    options,
    pmViews: { main },
  } = useContext(WaxContext)

  const { app } = useContext(ApplicationContext)
  const waxMenuConfig = app.config.get('config.MenuService')
  let fullScreenStyles = {}

  const menuContainsTrackTools = !!waxMenuConfig[0].toolGroups.find(
    menu => menu === 'TrackingAndEditing',
  )

  if (options.fullScreen) {
    fullScreenStyles = {
      backgroundColor: '#fff',
      height: '100%',
      left: '0',
      margin: '0',
      padding: '0',
      position: 'fixed',
      top: '0',
      width: '100%',
      zIndex: '99999',
    }
  }

  const commentsTracksCount =
    main && DocumentHelpers.getCommentsTracksCount(main)

  const trackBlockNodesCount =
    main && DocumentHelpers.getTrackBlockNodesCount(main)

  const showTrackControls =
    menuContainsTrackTools || commentsTracksCount + trackBlockNodesCount > 0

  useEffect(() => {
    // Re-check on window resize
    window.addEventListener('resize', checkOverflow)
    return () => window.removeEventListener('resize', checkOverflow)
  }, [])

  useEffect(() => {
    if (editorLoading) {
      document.getElementById('toolbar').classList.remove('scrollable')
    } else {
      setTimeout(() => {
        checkOverflow()
      }, 1)
    }
  }, [editorLoading])

  useEffect(() => {
    // make comments visible when adding a new comment and they are hidden
    if (previousComments?.length < savedComments?.length) {
      setShowComments(true)
    }
  }, [savedComments])

  const toggleMetadata = which => {
    if (viewMetadata !== which) {
      setViewMetadata(which)

      if (selectedChapterId) {
        onChapterClick(selectedChapterId)
        setLastSelectedChapter(selectedChapterId)
      }
    } else {
      if (lastSelectedChapter) {
        setLastSelectedChapter(selectedChapterId)
        onChapterClick(lastSelectedChapter)
      }

      setViewMetadata('')
    }

    if (window.innerWidth < 600) {
      setBookPanelCollapsed(true)
    }
  }

  const handleChapterClick = chapterId => {
    if (viewMetadata !== '') setViewMetadata('')
    onChapterClick(chapterId)

    if (window.innerWidth < 600 && !bookPanelCollapsed) {
      setBookPanelCollapsed(true)
    }
  }

  const checkOverflow = () => {
    const toolbar = document.getElementById('toolbar')
    toolbar?.classList.remove('scrollable')

    // Check if the content overflows the container
    if (toolbar?.scrollWidth > toolbar?.clientWidth) {
      toolbar?.classList.add('scrollable') // Add class to align items to the start
    } else {
      toolbar?.classList.remove('scrollable') // Remove class to center items
    }

    if (window.innerWidth > 1400) {
      if (
        document.getElementById('commentToggle')?.classList.contains('hidden')
      ) {
        setShowComments(true)
        document.getElementById('commentToggle')?.classList.remove('hidden')
      }
    } else {
      document.getElementById('commentToggle')?.classList.add('hidden')
    }
  }

  const renderInformationBox = () => {
    switch (viewMetadata) {
      case 'metadata':
        return (
          <StyledMetadataForm
            canChangeMetadata={canEdit}
            initialValues={bookMetadataValues}
            onSubmitBookMetadata={onSubmitBookMetadata}
            onUploadBookCover={onUploadBookCover}
          />
        )
      case 'settings':
        return (
          <StyledSettingsForm
            aiEnabled={aiEnabled}
            bookId={bookId}
            bookSettings={settings}
            refetchBookSettings={getBookSettings}
          />
        )

      case 'members':
        return <UserInviteModal bookId={bookId} />

      default:
        return null
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <Wrapper id="wax-container" style={fullScreenStyles}>
        <TopMenu
          data-expanded={!mobileToolbarCollapsed}
          data-loading={editorLoading}
          id="toolbar"
          isHidden={viewMetadata}
        >
          <Button
            icon={
              mobileToolbarCollapsed ? <CaretDownFilled /> : <CaretUpFilled />
            }
            iconPosition="end"
            id="collapse"
            onClick={() => setMobileToolbarCollapsed(!mobileToolbarCollapsed)}
          >
            {mobileToolbarCollapsed ? 'Expand' : 'Collapse'}
          </Button>
          {!editorLoading ? <MainMenuToolBar /> : null}
        </TopMenu>
        <Main>
          {!options.fullScreen && (
            <LeftPanelWrapper>
              <CollapseContainer data-collapsed={bookPanelCollapsed}>
                <Button
                  aria-label="Collapse"
                  icon={<ToTopOutlined />}
                  onClick={() => setBookPanelCollapsed(!bookPanelCollapsed)}
                  type="text"
                />
              </CollapseContainer>
              <BookInformation
                bookId={bookId}
                showAiAssistantLink={aiEnabled && settings?.aiPdfDesignerOn}
                showKnowledgeBaseLink={aiEnabled && settings?.knowledgeBaseOn}
                toggleInformation={toggleMetadata}
                viewInformation={viewMetadata}
              />

              <BookPanel
                bookMetadataValues={bookMetadataValues}
                canEdit={canEdit}
                chapters={chapters}
                chaptersActionInProgress={chaptersActionInProgress}
                metadataModalOpen={metadataModalOpen}
                onAddChapter={onAddChapter}
                onBookComponentParentIdChange={onBookComponentParentIdChange}
                onBookComponentTypeChange={onBookComponentTypeChange}
                onChapterClick={handleChapterClick}
                onDeleteChapter={onDeleteChapter}
                onReorderChapter={onReorderChapter}
                onSubmitBookMetadata={onSubmitBookMetadata}
                onUploadChapter={onUploadChapter}
                selectedChapterId={selectedChapterId}
                setMetadataModalOpen={setMetadataModalOpen}
                setViewMetadata={setViewMetadata}
                subtitle={subtitle}
                title={title}
                viewMetadata={viewMetadata}
              />
            </LeftPanelWrapper>
          )}
          {viewMetadata !== '' ? (
            renderInformationBox()
          ) : (
            <EditorArea isFullscreen={options.fullScreen}>
              <WaxSurfaceScroll id="wax-surface-scroll">
                <EditorContainer selectedChapterId={selectedChapterId}>
                  {editorLoading ? (
                    <StyledSpin spinning={editorLoading} />
                  ) : (
                    <>
                      {selectedChapterId ? (
                        <WaxView {...rest} />
                      ) : (
                        <NoSelectedChapterWrapper>
                          {t('editor.noChapterSelected')}
                        </NoSelectedChapterWrapper>
                      )}
                      <TrackToolsContainer>
                        {savedComments.length > 0 && (
                          <ToggleComments id="commentToggle">
                            <Checkbox
                              checked={showComments}
                              onChange={e => setShowComments(e.target.checked)}
                            >
                              SHOW COMMENTS
                            </Checkbox>
                          </ToggleComments>
                        )}
                        {showTrackControls && (
                          <TrackTools>
                            {commentsTracksCount + trackBlockNodesCount}{' '}
                            SUGGESTIONS
                            <TrackOptions>
                              <CommentTrackToolBar />
                            </TrackOptions>
                          </TrackTools>
                        )}
                      </TrackToolsContainer>
                      {showComments && (
                        <CommentsContainer>
                          <RightArea area="main" />
                        </CommentsContainer>
                      )}
                    </>
                  )}
                </EditorContainer>
              </WaxSurfaceScroll>
            </EditorArea>
          )}
        </Main>
      </Wrapper>
    </ThemeProvider>
  )
}

LuluLayout.propTypes = {
  customProps: PropTypes.shape().isRequired,
}

export default LuluLayout
